// counter
jQuery(document).ready(function ($) {
  $(".counter").counterUp({
    delay: 10,
    time: 1000,
  });
});

document.addEventListener("DOMContentLoaded", function () {
  new Splide(".splide", {
    type: "loop",
    perPage: 3,
    autoplay: true,
  }).mount();
});

document.addEventListener("DOMContentLoaded", function () {
  new Splide("#splide", {
    direction: "ttb",
    height: "350px",
  }).mount();
});

document.addEventListener("DOMContentLoaded", function () {
  new Splide("#splide2", {
    direction: "ttb",
    height: "350px",
  }).mount();
});

document.addEventListener("DOMContentLoaded", function () {
  new Splide("#splide3", {
    direction: "ttb",
    height: "450px",
  }).mount();
});
// CARD-DECK ON HOVER
let cardDeck = document.querySelectorAll(".card-deck");
let workerName = document.querySelectorAll(".workerName");
let cardFooter = document.querySelectorAll(".card-footer");

for (let i = 0; i < cardDeck.length; i++) {
  cardDeck[i].addEventListener("mouseover", function () {
    cardFooter[i].classList.add("card-footer__class");
    workerName[i].classList.add("card-footer__class__h3");
  });
}

for (let i = 0; i < cardDeck.length; i++) {
  cardDeck[i].addEventListener("mouseout", function () {
    cardFooter[i].classList.remove("card-footer__class");
    workerName[i].classList.remove("card-footer__class__h3");
  });
}

// accrodion

let cardHeader = document.querySelectorAll(".card-header");
let btnLink = document.querySelectorAll(".card-header button");
let card = document.querySelectorAll("#accordion .card");

for (let i = 0; i < card.length; i++) {
  card[i].addEventListener("click", function () {
    cardHeader[i].classList.add("cardHeaderColor");
    btnLink[i].classList.add("btnLinkColor");
  });
}

// /header
var mainListDiv = document.getElementById("mainListDiv"),
  mediaButton = document.getElementById("mediaButton");

mediaButton.onclick = function () {
  "use strict";

  mainListDiv.classList.toggle("show_list");
  mediaButton.classList.toggle("active");
};

//
$(".responsive").slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
   slidesToScroll: 1,
  autoplay: true,
  arrows: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        arrows: true,
        // dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 395,
      settings: {
        arrows: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});
